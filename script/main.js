function unpack(rows, key){
    return(rows.map((row) => {
        return row[key];
    }))
}

function draw_1(div, rows) {
    const pod_lis_frequency = unpack(rows, 'pod_lis_frequency');
    // console.log("test1");
    // console.log(pod_lis_frequency);
    const trace = {
        labels: pod_lis_frequency,
        type: 'pie',
        opacity : 0,
        marker: {
            colors: ['#2ca02c', '#1f77b4', '#ffff00', '#d62728', '#ff8000']
          },
        hoverlabel: {
            font: {
              family: 'Arial, sans-serif',
              size: 16,
              color: 'black',
            },
        },
    }  

    const layout = {     
        width : 500,
        height: 500,
        showlegend: false,
        paper_bgcolor: "rgba(0,0,0,0)",
    }

    Plotly.newPlot(div, [trace], layout);
}

function draw_2(div, rows) {
    const pod_variety_satisfaction = unpack(rows, 'pod_variety_satisfaction');
    // console.log("test1");
    // console.log(pod_variety_satisfaction);

    const trace = {
        opacity : 0,
        x: pod_variety_satisfaction,
        name: '',
        type: 'histogram',
        histnorm: 'percent',
        hovertemplate: '<b>%{x}</b>: %{y:.0f}%',
        hoverlabel: {
            font: {
              family: 'Arial, sans-serif',
              size: 16,
              color: 'black',
            }
        },
        marker: {
            color: ['#ffff00', '#1f77b4', '#2ca02c', '#ff8000', '#d62728'],
            line_color: 'white',
            line_width: 1
        },
    }  

    const layout = {    
        width : 865,
        height: 500,
        showlegend: false,
        xaxis: {
            categoryorder: 'array',
            categoryarray: ['Ok', 'Satisfied', 'Very Satisfied', 'Dissatisfied', 'Very Dissatisfied'],
            showgrid: false,
            title: 'Satisfactions des utilisateurs',
        },
        yaxis: {
            range: [0, 100], // Define the y-axis range on [0, 100]
            tick0: 10, // Set the starting point for ticks to 10
            dtick: 10, // Set the interval between ticks to 10
            showgrid: false,
            title: 'Pourcentage de satisfaction',
          },
        bargap: 0.5,
        paper_bgcolor: 'rgba(0,0,0,0)',
        plot_bgcolor: 'rgba(0,0,0,0)',
    }

    Plotly.newPlot(div, [trace], layout);
}

function draw_3(div, rows) {

    let pod_frequency = ["Rarely", "Once a week", "Several times a week", "Daily"];
    let duration = ["Shorter", "Both", "Longer"];
    let colors = ['#ff8000', '#ffff00','#1f77b4','#2ca02c'];
    let data = [];

    pod_frequency.forEach(function (duree, index) {
        duration.forEach(function (durée) {
            let filteredRows = rows.filter(row => row.pod_lis_frequency === duree && row.preffered_pod_duration === durée);
            let trace = {
                x: [durée],
                y: [filteredRows.length],
                name: '',
                type: 'bar',
                marker: { color: colors[index] },
                hovertemplate: '<b>'+ duree + '</b> : ' + filteredRows.length,
                opacity: 0,
                hoverlabel: {
                    font: {
                    family: 'Arial, sans-serif',
                    size: 16,
                    color: 'black',
                    },
                },
            };
      data.push(trace);
        });
    });

    let layout = {
        showlegend: false,
        width: 1000,
        height: 500,
        barmode: 'stack',
        xaxis: { title: 'Longueur',
        showgrid: false,
        },
        yaxis: { title: 'Count',
        showgrid: false,
        },
        bargap: 0.4,
        paper_bgcolor: 'rgba(0,0,0,0)',
        plot_bgcolor: 'rgba(0,0,0,0)',
    };

  Plotly.newPlot(div, data, layout);
}
async function main(){
    const rows = await d3.csv("dataset/Spotify_users_data.csv");
    // console.log(rows);
    const plot = document.getElementById('plot');
    draw_1(plot, rows);
    const plot2 = document.getElementById('plot_2');
    draw_2(plot2, rows);
    const plot3 = document.getElementById('plot_3');
    draw_3(plot3, rows);
}

main();
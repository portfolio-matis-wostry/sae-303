# SAE 303

Ce site était un projet où nous devions effectuer des analyses graphiques et des designs graphiques.

Pour cela, nous avons utilisé Plotly pour créer des graphiques sur notre site. J'ai également créé des graphiques sur Illustrator afin de créer des SVG à placer sur le site.

Le site est accessible avec l'extension "Go Live" présente sur Visual Studio Code. Sinon, la lisibilité du site est impossible à certains endroits.
